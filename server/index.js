import express from 'express';
import db from './config/db.js';
import contactRoutes from './routes/index.js';
import cors from 'cors';
const app = express();
const port = 8000;

app.use(cors());
app.use(express.json())

app.use('/contacts', contactRoutes);

app.listen(port, () => console.log(`Server is running at http://localhost:${port}`))