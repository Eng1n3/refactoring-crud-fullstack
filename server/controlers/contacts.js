import Person from '../models/person.js';
import {validationResult} from 'express-validator';

export const getContactsAll = async (req, res) => {
    try {
        const person = await Person.find();
        res.json(person);
    } catch (err) {
        res.json({message: err.message});
    };
}

export const getContactsDetail = async (req, res) => {
    const id = req.params.id;
    try {
        const person = await Person.findOne({_id: id})
        res.send({person});
    } catch (err) {
        res.send(err)
    }
}

export const getContactsPost = async (req, res) => {
    const errors = validationResult(req);
    if ( !errors.isEmpty() ) {
        return res.send(errors)
    }
    await Person.insertMany(req.body)
    res.json({message: "Data berhasil ditambahkan"})
}

export const getContactsPatch = async (req, res) => {
    const errors = validationResult(req);
    if ( !errors.isEmpty() ) {
        return res.send(errors)
    }
    await Person.updateOne(
        { _id: req.params.id},
        { $set: {
            "nama": req.body.nama,
            "email": req.body.email,
            "nomor": req.body.nomor
        }}
    )
    res.send({message: "Data berhasil di update"});
}

export const getContactsDelete = async (req, res) => {
    try {
        await Person.deleteOne({_id: req.params.id})
        res.send({message: "Data berhasil di hapus"});
    } catch (err) {
        res.send({message: err.message})
    }
}