import express from "express";
import { 
    getContactsAll,
    getContactsDelete,
    getContactsDetail,
    getContactsPatch,
    getContactsPost
} from "../controlers/contacts.js";
import { validatePatch, validatePost } from "../validators/validate.js";

const router = express.Router();

router.get('/', getContactsAll);

router.get('/:id', getContactsDetail);

router.post('/', validatePost, getContactsPost);

router.patch('/:id', validatePatch, getContactsPatch);

router.delete('/:id', getContactsDelete);

export default router;