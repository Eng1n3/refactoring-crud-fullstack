import mongoose from "mongoose";

const db = mongoose.connect('mongodb://localhost:27017/contact');

mongoose.connection.on('connected', function () {  
    console.log('Database Connection success');
}); 

// If the connection throws an error
mongoose.connection.on('error',function (err) {  
    console.log('Database Connection error: ' + err);
}); 

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {  
    console.log('Database Connection disconnected'); 
});

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function() {  
    mongoose.connection.close(function () { 
        console.log('Database Connection disconnected through app termination'); 
        process.exit(0); 
    }); 
});

export default db;