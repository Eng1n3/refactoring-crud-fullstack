import mongoose from 'mongoose';

const Person = mongoose.model('Person', {
    nama: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true
    },
    nomor: {
        type: String,
    }
})

export default Person;