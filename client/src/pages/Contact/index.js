import React from 'react';
import { ListContact } from '../../components';
import { useHistory, useRouteMatch } from 'react-router-dom';

const Contact = () => {
    let match = useRouteMatch();
    const history = useHistory();
    return (
        <React.Fragment>
            <div className="row">
                <div className="col-md-12">
                    <h1>Ini contact page</h1>
                    <button onClick={() => history.push(`${match.path}/create`)} className="btn btn-md btn-primary">Tambahkan data</button>
                    <div className="table-responsive">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Nomor</th>
                                    <th scope="col">Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                <ListContact />
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Contact;
