import React, {Component} from 'react'

const DetailPost = () => {
    return (
            <div className="row">
                <div className="col-md-4">
                    <h1>Detail Contact</h1>
                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title">Adam Brilian</h5>
                            <p className="card-text">adam@gmail.com</p>
                            <p className="card-text">089630682130</p>
                            <button className="btn btn-warning badge">Update</button>
                            <button className="btn btn-danger badge">Delete</button>
                            <button className="btn btn-sm btn-primary d-block mt-3">kembali ke daftar kontak</button>
                        </div>
                    </div>
                </div>
            </div>
    )
}

export default DetailPost;
