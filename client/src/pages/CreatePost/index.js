import React from 'react'
import {Gap, Input} from "../../components";

const CreatePost = () => {
    return (
        <React.Fragment>
            <div className="row">
                <div className="col-md-6">
                    <h1>Create dan Update Data</h1>
                    <form>
                        <Input label="nama" required type="text" />
                        <Gap height={20} />
                        <Input label="email" required type="email" />
                        <Gap height={20} />
                        <Input label="nomor" required type="text" />
                        <Gap height={20} />
                        <button className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </React.Fragment>
    )
}

export default CreatePost;
