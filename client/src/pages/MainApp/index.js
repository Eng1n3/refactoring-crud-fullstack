import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from '../Home';
import About from '../About';
import Contact from '../Contact';
import CreatePost from '../CreatePost';
import DetailPost from '../DetailPost';

const MainApp = () => {
    return (
        <React.Fragment>
            <Router>
                <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed mb-3">
                    <div className="container">
                        <Link className="navbar-brand" to="/">Data</Link>
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarNav">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <Link to="/" className="nav-link active" aria-current="page">Home</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to="/about" className="nav-link" aria-current="page">About</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to="/contact" className="nav-link" aria-current="page">Contact</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div className="container">
                    <Switch>
                        <Route path="/" exact>
                            <Home />
                        </Route>
                        <Route path="/contact" exact>
                            <Contact />
                        </Route>
                        <Route>
                            <CreatePost path="/contact/create"/>
                        </Route>
                        <Route path="/about">
                            <About />
                        </Route>
                    </Switch>
                </div>
            </Router>
        </React.Fragment>
    )
}

export default MainApp;