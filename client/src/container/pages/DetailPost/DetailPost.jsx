import React, {Component, Fragment} from "react";
import "./DetailPost.css";

class DetailPost extends Component {
    constructor (props) {
        super(props)
        this.state = {
            post: {
                nama: "",
                email: "",
                nomor: ""
            }
        }
    }

    getDataAPI = () => {
        const id = this.props.match.params.id;
        fetch(`http://localhost:8000/contacts/${id}`)
        .then(response => response.json())
        .then(response => {
            this.setState({
                post: response.person
            })
        })
        .catch(err => console.log(err));
    }

    goUrlContact = () => {
        return this.props.history.push(`/contact`);
    }

    goUrlUpdate = () => {
        return this.props.history.push(`/update-post/${this.state.post._id}`)
    }

    handleDelete = async () => {
        const konfirm = window.confirm('Yakin?');
        if (konfirm) {
            await fetch(`http://localhost:8000/contacts/${this.state.post._id}`, {
                method: "DELETE",
            })
            .then(response => response.json())
            .then(response => this.props.history.push('/contact'))
            .catch(err => console.error(err));
        }
    }

    componentDidMount() {
        this.getDataAPI();
    }

    render() {
        return (
            <Fragment>
                <div className="container">
                    <h1>Detail Contact</h1>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="card">
                                <div className="card-body">
                                    <h5 className="card-title">{this.state.post.nama}</h5>
                                    <p className="card-text">{this.state.post.email}</p>
                                    <p className="card-text">{this.state.post.nomor}</p>
                                    <button onClick={this.goUrlUpdate} className="btn btn-warning badge">Update</button>
                                    <button onClick={this.handleDelete} className="btn btn-danger badge">Delete</button>
                                    <button onClick={this.goUrlContact} className="btn btn-sm btn-primary d-block mt-3">kembali ke daftar kontak</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default DetailPost;