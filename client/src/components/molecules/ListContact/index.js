import React from 'react';
import {useRouteMatch, useHistory} from "react-router-dom";

const ListContact = () => {
    let match = useRouteMatch();
    const history = useHistory();
    return (
            <tr>
                <th scope="row">1</th>
                <td>Adam Brilian</td>
                <td>adam@gmail.com</td>
                <td>089630682130</td>
                <td>
                    <button className="btn btn-md btn-success badge rounded-pill">
                        Detail
                    </button>
                </td>
            </tr>
    )
}

export default ListContact;
