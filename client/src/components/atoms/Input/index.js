import React from 'react'

const Input = ({label, ...rest}) => {
    return (
        <div>
            <label htmlFor={label} className="form-label">{label}</label>
            <input className="form-control" id={label} name={label} {...rest} />
        </div>
    )
}

export default Input
