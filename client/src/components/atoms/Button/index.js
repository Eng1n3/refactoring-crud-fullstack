import React from 'react'

const index = ({name, ...rest}) => {
    return <button {...rest}>{name}</button>
}

export default index
